(function (Drupal) {
  'use strict';

  const AUTHENTICATE_API = '/api/bankid/authenticate';
  const COLLECT_API = '/api/bankid/collect/';
  const CANCEL_API = '/api/bankid/cancel/';
  var orderRef, transactionId, qrStartSecret, qrStartToken, autoStartToken;
  var self;
  var time = 0;
  var stopPolling = false;

  Drupal.behaviors.bankIDAuthenticate = {
    attach: function (context) {
      self = this;

      document.querySelectorAll('.bankid-dialog').forEach(function() {
        stopPolling = false;
        // Start authentication process.
        self.authenticate();
      });

      document.querySelectorAll('.ui-dialog-titlebar-close').forEach(function(button) {
        button.addEventListener('click', function() {
          // Cancel authentication process.
          self.cancel();
        });
      });

    },
    ajaxRequest: function (method, url, callback) {
      var xhr = new XMLHttpRequest();
      xhr.onreadystatechange = function () {
        if (xhr.readyState === XMLHttpRequest.DONE) {
          if (xhr.status === 200) {
            callback(null, JSON.parse(xhr.responseText));
          } else {
            callback(new Error('Request failed with status: ' + xhr.status));
          }
        }
      };
      xhr.open(method, url);
      xhr.send();
    },
    authenticate: function () {
      if (!stopPolling) {
        self.ajaxRequest('GET', AUTHENTICATE_API, function (err, data) {
          if (!err) {
            orderRef = data.orderRef;
            transactionId = data.transactionId;
            qrStartSecret = data.qrStartSecret;
            qrStartToken = data.qrStartToken;
            autoStartToken = data.autoStartToken;
            time = 0;

            if (data.message.length !== 0) {
              document.getElementById('bankid-status-message').textContent = data.message;
            }

            if (!stopPolling) {
              // Start collecting status
              self.collect();
              setTimeout(self.authenticate, 25000);
            }
          }
        });
      }
    },
    collect: function () {
      if (typeof orderRef !== 'undefined' && !stopPolling) {
        self.ajaxRequest('GET', COLLECT_API + orderRef, function (err, data) {
          var message = document.getElementById('bankid-status-message');

          if (!err && data.status === 'pending') {
            var throbber = document.getElementById('bankid-throbber');
            if (throbber) {
              throbber.classList.add('visually-hidden');
            }

            self.generateQRCode();
            self.generateAppLink();
            time++;
            // Continue collecting after 2 seconds
            setTimeout(self.collect, 2000);
          }
          else if (!err && data.status === 'complete') {
            stopPolling = true;
            // Set response value
            document.querySelector('.bankid-response').value = JSON.stringify(data);
            // Manually trigger click event
            document.querySelectorAll('.login-submit').forEach(function(element) {
              element.click();
            });
          }
          else if (!err && data.status === 'failed') {
            stopPolling = true;
            // Clear QR code.
            var canvas = document.getElementById('qrcode');
            if (canvas) {
              var ctx = canvas.getContext('2d');
              ctx.clearRect(0, 0, canvas.width, canvas.height);
            }
            // Clear app link.
            var link = document.getElementById('bankid-app-link');
            if (link) {
              link.innerHTML = '';
            }
          }

          if (!err && message) {
            message.textContent = data.message;
          }

        });
      }
    },
    cancel: function () {
      stopPolling = true;
      self.ajaxRequest('GET', CANCEL_API + orderRef, function (err, data) {});
    },
    generateQRCode: function () {
      // Create the hash using CryptoJS.HmacSHA256
      var hash = CryptoJS.HmacSHA256(time.toString(), qrStartSecret);
      // Update QR code.
      var qrcode = document.getElementById('qrcode');
      if (qrcode) {
        qrcode.classList.remove('visually-hidden');
      }

      new QRious({
        element: qrcode,
        size: 256,
        value: `bankid.${qrStartToken}.${time}.${hash}`
      });
    },
    generateAppLink: function () {
      var link = document.getElementById('bankid-app-link');
      if (link) {
        var appLink = document.createElement('a');
        appLink.textContent = Drupal.t('Open BankID app on this device');
        appLink.href = 'bankid:///?autostarttoken=' + autoStartToken + '&redirect=null';
        appLink.classList.add('button');
        link.innerHTML = ''
        link.appendChild(appLink);
      }
    }
  };
})(Drupal);