# BankID

The BankID module for Drupal provides seamless integration with the 
[BankID.com](https://www.bankid.com/) authentication system, enabling users to
authenticate themselves on your Drupal site using their BankID credentials. This
module facilitates secure and reliable user verification, leveraging the
widespread and trusted BankID infrastructure.

Key Features:

1. **User Authentication:** Allows users to log in or register on your Drupal
site using their BankID, enhancing security and simplifying the authentication
process.
2. **Configuration Options:** Provides a flexible configuration interface to set
up and customize BankID settings according to your specific requirements. This
includes options for test and production environments.
3. **API Integration:** Integrates with the BankID API, ensuring robust
communication between your Drupal site and the BankID service for authentication
requests and responses.
4. **User Experience:** Enhances the user experience by providing a 
straightforward and intuitive BankID authentication flow, reducing the need for
traditional username and password logins.
5. **Security:** Ensures high security standards by leveraging the BankID
system, which is widely recognized for its security and reliability in user
authentication.

## Table of contents

- Requirements
- Installation
- Configuration
- Creating a Custom Plugin
- Plans for the future
- Maintainers

## Requirements

- The [Key](https://www.drupal.org/project/key) module
- The [External Authentication](https://www.drupal.org/project/externalauth) module
- A BankID account is also needed (contact your bank for more info).

## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration

Configure the BankID module under BankID Settings (/admin/config/system/bankid)
and then add a new BankID Authenticate block from the Block layout
(/admin/structure/block) page. It´s also possible to add the login button in a
custom twig by using
[Twig Tweak](https://www.drupal.org/project/twig_tweak):

`{{ drupal_form('Drupal\\bankid\\Form\\BankIDAuthenticateForm') }}`


### Test environment
Follow the instructions on
[Test BankID](https://www.bankid.com/en/utvecklare/test/skaffa-testbankid) and
create a 
[BankID for test](https://www.bankid.com/en/utvecklare/test/skaffa-testbankid/test-bankid-get)
using a test person number from
[Swedish Tax Agency](https://skatteverket.entryscape.net/rowstore/dataset/b4de7df7-63c0-4e7e-bb59-1f156a591763/html).

The test environment doesn´t need any further configuration, the provided Keys
and certificates in assets should be enough to get it running.

### Production environment
Order certificates from your bank and follow the instructions under
[Production environment](https://www.bankid.com/en/utvecklare/guider/teknisk-integrationsguide/miljoer).

When all certificates are received, upload them to a folder outside of the web
root on the server and Add key (admin/config/system/keys/add) for each. Then add
the keys to production environment under BankID Settings
(/admin/config/system/bankid).

## Creating a Custom Plugin
Creating a Custom Plugin by Extending IntegrationBase for Custom Integrations.

To integrate the BankID module with a third-party system like a CRM, follow 
these steps:

1. **Create a Custom Module:** Set up a custom module if you don´t have one.
2. **Define the Plugin:** Create a PHP file in src/Plugin/BankID/ within your 
module, extending IntegrationBase.
3. **Implement Methods:**
   - **getUser:** Get the user from the response.
   - **createUser:** Create the user from the response.
4. **Register the Plugin:** Enable your module so Drupal registers the plugin.
5. **Configure the Integration:** In BankID settings, select and configure your 
custom integration.
6. **Test the Integration:** Ensure the plugin works with the CRM, handling 
authentication and data sync correctly.

This allows seamless authentication integration with external systems like CRMs.

## Plans for the future

Our long-term plans are to implementent/improve on the following:

* Improve on code compliance and standards
* ~~Integrate with [External Authentication](https://www.drupal.org/project/externalauth)~~ - Done as of 1.0.3
* ~~Enable users provisioned via the *BankID* module to change email adress by 
implementing custom `ProtectedUserFieldContstraint`~~ - Done as of 1.0.2

## Maintainers

Current maintainers:

- [Peter Törnstrand](https://www.drupal.org/u/peter-tornstrand)
- [Thomas Alsén](https://www.drupal.org/u/thomas-alsen)

Supporting organizations:

The Drupal module BankID was developed by 
[Thomas Alsén](https://www.drupal.org/u/thomas-alsen) and sponsored by
[HAPPINESS WEB AGENCY](http://www.happiness.se) in the beautiful city Stockholm,
Sweden.
