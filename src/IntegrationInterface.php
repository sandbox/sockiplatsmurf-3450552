<?php

declare(strict_types=1);

namespace Drupal\bankid;

use Drupal\Core\Entity\EntityInterface;
use Drupal\user\UserInterface;

/**
 * An interface for all Integration type plugins.
 */
interface IntegrationInterface {

  /**
   * Provide the Integration label.
   *
   * @return string
   *   A string label of the Integration.
   */
  public function getLabel(): string;

  /**
   * Get the user from the response.
   *
   * @param string $response
   *   The response from the BankID service.
   *
   * @return Drupal\user\UserInterface\UserInterface|null
   *   The user object.
   */
  public function getUser(string $response): EntityInterface|NULL;

  /**
   * Create the user from the response.
   *
   * @param string $response
   *   The response from the BankID service.
   *
   * @return Drupal\user\UserInterface
   *   The user object.
   */
  public function createUser(string $response): UserInterface;

  /**
   * Get the hash of a string.
   *
   * @param string $str
   *   The string to hash.
   *
   * @return string
   *   The hashed string.
   */
  public function hash(string $str): string;

}
