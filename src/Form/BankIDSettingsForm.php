<?php

namespace Drupal\bankid\Form;

use Drupal\Core\Url;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\bankid\IntegrationManager;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\Exception\RouteNotFoundException;

/**
 * Configure BankID settings.
 */
class BankIDSettingsForm extends ConfigFormBase {

  /**
   * The integration manager.
   *
   * @var \Drupal\bankid\IntegrationManager
   */
  protected $integrationManager;

  /**
   * Constructs a new BankIDSettingsForm.
   *
   * @param \Drupal\bankid\IntegrationManager $integration_manager
   *   The integration manager.
   */
  public function __construct(IntegrationManager $integration_manager) {
    $this->integrationManager = $integration_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.bankid.integration')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'bankid_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['bankid.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('bankid.settings');

    $form['environment'] = [
      '#type' => 'select',
      '#title' => $this->t('Environment'),
      '#options' => [
        'prod' => $this->t('Production'),
        'test' => $this->t('Test'),
      ],
      '#default_value' => $config->get('environment'),
      '#description' => $this->t('Select the environment to use.'),
      '#required' => TRUE,
    ];
    $form['integration'] = [
      '#type' => 'select',
      '#title' => $this->t('BankID integration'),
      '#options' => $this->buildIntegrationOptions(),
      '#default_value' => $config->get('integration'),
      '#required' => TRUE,
      '#description' => $this->t('Select the integration plugin to use.'),
    ];
    $form['create_user'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Create user'),
      '#default_value' => $config->get('create_user'),
      '#description' => $this->t('If checked, a new user will be created on login if the user does not exist.'),
    ];
    $form['redirect_path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Redirect path'),
      '#description' => $this->t('Enter the path to redirect users to after login. Example: /user/dashboard'),
      '#default_value' => $config->get('redirect_path'),
      '#required' => TRUE,
    ];

    $description = $this->t('Choose an available key. If the desired key is not listed, <a href=":link">create a new key</a>.', [
      ':link' => Url::fromRoute('entity.key.add_form')->toString(),
    ]);
    $form['test'] = [
      '#type' => 'details',
      '#title' => $this->t('Bankid test API') . ' (' . $config->get('test.api_base_url') . ')',
      '#open' => TRUE,
      '#description' => $description,
      '#states' => [
        'visible' => [
          'select[name="environment"]' => ['value' => 'test'],
        ],
      ],
    ];
    $form['prod'] = [
      '#type' => 'details',
      '#title' => $this->t('Bankid production API') . ' (' . $config->get('prod.api_base_url') . ')',
      '#open' => TRUE,
      '#description' => $description,
      '#states' => [
        'visible' => [
          'select[name="environment"]' => ['value' => 'prod'],
        ],
      ],
    ];

    $form['test']['test_rp_certificate'] = [
      '#type' => 'key_select',
      '#title' => $this->t('SSL certificate (RP certificate for test)'),
      '#key_filters' => [
        'type' => 'authentication',
        'provider' => 'file',
      ],
      '#default_value' => $config->get('test.rp_certificate') ?? '',
      '#key_description' => FALSE,
      '#description' => $this->t('Use key type: :authentication and key provider: :file.', [
        ':authentication' => $this->t('Authentication'),
        ':file' => $this->t('File'),
      ]),
      '#states' => [
        'required' => [
          'select[name="environment"]' => ['value' => 'test'],
        ],
      ],
    ];
    $form['test']['test_rp_passphrase'] = [
      '#type' => 'key_select',
      '#title' => $this->t('Passphrase (for RP certificate on test)'),
      '#key_filters' => [
        'type' => 'authentication',
        'provider' => [
          'config',
          'file',
        ],
      ],
      '#default_value' => $config->get('test.rp_passphrase') ?? '',
      '#key_description' => FALSE,
      '#description' => $this->t('Use key type: :authentication and key provider: :file or :config.', [
        ':authentication' => $this->t('Authentication'),
        ':file' => $this->t('File'),
        ':config' => $this->t('Configuration'),
      ]),
      '#states' => [
        'required' => [
          'select[name="environment"]' => ['value' => 'test'],
        ],
      ],
    ];
    $form['test']['test_issuer_of_server_certificate'] = [
      '#type' => 'key_select',
      '#title' => $this->t('Issuer of server certificate (for test)'),
      '#key_filters' => [
        'type' => 'authentication',
        'provider' => 'file',
      ],
      '#default_value' => $config->get('test.issuer_of_server_certificate') ?? '',
      '#key_description' => FALSE,
      '#description' => $this->t('Use key type: :authentication and key provider: :file.', [
        ':authentication' => $this->t('Authentication'),
        ':file' => $this->t('File'),
      ]),
      '#states' => [
        'required' => [
          'select[name="environment"]' => ['value' => 'test'],
        ],
      ],
    ];

    $form['prod']['prod_rp_certificate'] = [
      '#type' => 'key_select',
      '#title' => $this->t('SSL certificate (RP certificate)'),
      '#key_filters' => [
        'type' => 'authentication',
        'provider' => 'file',
      ],
      '#default_value' => $config->get('prod.rp_certificate') ?? '',
      '#key_description' => FALSE,
      '#description' => $this->t('Use key type: :authentication and key provider: :file.', [
        ':authentication' => $this->t('Authentication'),
        ':file' => $this->t('File'),
      ]),
      '#description' => '<div>' . $this->t('Select the production certificate to use.') . '</div>',
      '#states' => [
        'required' => [
          'select[name="environment"]' => ['value' => 'prod'],
        ],
      ],
    ];
    $form['prod']['prod_rp_passphrase'] = [
      '#type' => 'key_select',
      '#title' => $this->t('Passphrase (for RP certificate on production)'),
      '#key_filters' => [
        'type' => 'authentication',
        'provider' => [
          'config',
          'file',
        ],
      ],
      '#default_value' => $config->get('prod.rp_passphrase') ?? '',
      '#key_description' => FALSE,
      '#description' => $this->t('Use key type: :authentication and key provider: :file or :config.', [
        ':authentication' => $this->t('Authentication'),
        ':file' => $this->t('File'),
        ':config' => $this->t('Configuration'),
      ]),
      '#states' => [
        'required' => [
          'select[name="environment"]' => ['value' => 'prod'],
        ],
      ],
    ];
    $form['prod']['prod_issuer_of_server_certificate'] = [
      '#type' => 'key_select',
      '#title' => $this->t('Issuer of server certificate (for production)'),
      '#key_filters' => [
        'type' => 'authentication',
        'provider' => 'file',
      ],
      '#default_value' => $config->get('prod.issuer_of_server_certificate') ?? '',
      '#key_description' => FALSE,
      '#description' => $this->t('Use key type: :authentication and key provider: :file.', [
        ':authentication' => $this->t('Authentication'),
        ':file' => $this->t('File'),
      ]),
      '#states' => [
        'required' => [
          'select[name="environment"]' => ['value' => 'prod'],
        ],
      ],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('bankid.settings')
      ->set('environment', $form_state->getValue('environment'))
      ->set('integration', $form_state->getValue('integration'))
      ->set('create_user', $form_state->getValue('create_user'))
      ->set('redirect_path', $form_state->getValue('redirect_path'))
      ->set('test.rp_certificate', $form_state->getValue('test_rp_certificate'))
      ->set('test.rp_passphrase', $form_state->getValue('test_rp_passphrase'))
      ->set('test.issuer_of_server_certificate', $form_state->getValue('test_issuer_of_server_certificate'))
      ->set('prod.rp_certificate', $form_state->getValue('prod_rp_certificate'))
      ->set('prod.rp_passphrase', $form_state->getValue('prod_rp_passphrase'))
      ->set('prod.issuer_of_server_certificate', $form_state->getValue('prod_issuer_of_server_certificate'))
      ->save();
    parent::submitForm($form, $form_state);
  }

  /**
   * Validate that the redirect path is a valid internal route or path.
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $redirect_path = $form_state->getValue('redirect_path');

    try {
      if (!str_starts_with($redirect_path, '/')) {
        throw new \InvalidArgumentException("The user-entered string '$redirect_path' must begin with a '/'.");
      }

      // Use Url::fromUserInput() to validate the path.
      $url = Url::fromUserInput($form_state->getValue('redirect_path'));

      // Check if the generated URL is a valid route.
      if (!$url->isRouted()) {
        $form_state->setErrorByName('redirect_path', $this->t('The path %path does not exist.', ['%path' => $redirect_path]));
      }
    }
    catch (RouteNotFoundException $e) {
      // Catch any exceptions and set an error if the route/path is invalid.
      $form_state->setErrorByName('redirect_path', $this->t('The path %path does not exist.', ['%path' => $redirect_path]));
    }
    catch (\InvalidArgumentException $e) {
      // Catch any exceptions and set an error if the route/path is invalid.
      $form_state->setErrorByName('redirect_path', $e->getMessage());
    }

    parent::validateForm($form, $form_state);
  }

  /**
   * Builds the integration options.
   *
   * @return array
   *   An array of integration options.
   */
  private function buildIntegrationOptions() {
    $options = [];
    $all_plugins = $this->integrationManager->getDefinitions();
    uasort($all_plugins, function ($a, $b) {
      return strnatcasecmp($a['label'], $b['label']);
    });
    foreach ($all_plugins as $definition) {
      /** @var \Drupal\bankid\IntegrationInterface $plugin */
      $plugin = $this->integrationManager->createInstance($definition['id']);
      $options[$plugin->getPluginId()] = $plugin->getLabel();
    }
    return $options;
  }

}
