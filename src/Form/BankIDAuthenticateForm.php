<?php

namespace Drupal\bankid\Form;

use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Url;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\bankid\IntegrationManager;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Ajax\OpenModalDialogCommand;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\externalauth\AuthmapInterface;
use Drupal\externalauth\ExternalAuthInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * BankID Authenticate form.
 */
class BankIDAuthenticateForm extends FormBase {

  /**
   * The integration manager.
   *
   * @var \Drupal\bankid\IntegrationManager
   */
  protected IntegrationManager $integrationManager;

  /**
   * The configuration.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected ImmutableConfig $config;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected AccountInterface $currentUser;

  /**
   * The externalauth service.
   *
   * @var \Drupal\externalauth\ExternalAuthInterface
   */
  protected ExternalAuthInterface $externalAuth;

  /**
   * The authmap service.
   *
   * @var \Drupal\externalauth\AuthmapInterface
   */
  protected AuthmapInterface $authmap;

  /**
   * Constructs a new BankIDAuthenticateForm.
   *
   * @param \Drupal\bankid\IntegrationManager $integration_manager
   *   The integration manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\externalauth\ExternalAuthInterface $externalAuth
   *   The externalauth service.
   * @param \Drupal\externalauth\AuthmapInterface $authmap
   *   The authmap service.
   */
  public function __construct(IntegrationManager $integration_manager, ConfigFactoryInterface $config_factory, AccountInterface $current_user, ExternalAuthInterface $externalAuth, AuthmapInterface $authmap) {
    $this->integrationManager = $integration_manager;
    $this->config = $config_factory->get('bankid.settings');
    $this->currentUser = $current_user;
    $this->externalAuth = $externalAuth;
    $this->authmap = $authmap;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.bankid.integration'),
      $container->get('config.factory'),
      $container->get('current_user'),
      $container->get('externalauth.externalauth'),
      $container->get('externalauth.authmap')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'bankid_authenticate_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    // If the user is already authenticated, show a logout button.
    if ($this->currentUser->isAuthenticated()) {
      $form['logout'] = [
        '#type' => 'button',
        '#value' => $this->t('Logout'),
        '#attributes' => [
          'onclick' => 'window.location.href="' . Url::fromRoute('user.logout')->toString() . '"; return false;',
        ],
      ];
      $form['#cache']['max-age'] = 0;
      return $form;
    }

    $form['auth'] = [
      '#type' => 'button',
      '#value' => $this->t('Login with BankID'),
      '#ajax' => [
        'progress' => [
          'type' => 'throbber',
          'message' => NULL,
        ],
        'callback' => '::auth',
      ],
    ];
    $form['response'] = [
      '#type' => 'hidden',
      '#default_value' => FALSE,
      '#attributes' => [
        'class' => [
          'bankid-response',
        ],
      ],
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#attributes' => [
        'class' => [
          'login-submit',
          'visually-hidden',
        ],
      ],
      '#value' => $this->t('Submit'),
    ];
    $form['#attached']['library'][] = 'bankid/bankid.authenticate';
    $form['#cache']['max-age'] = 0;

    return $form;
  }

  /**
   * Ajax callback to open BankID QR dialog.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   The ajax response.
   */
  public function authQrCode(array &$form, FormStateInterface $form_state): AjaxResponse {
    $ajax_response = new AjaxResponse();
    $title = $this->t('Mobile BankID');
    $build = [
      '#theme' => 'authenticate_dialog',
    ];
    $options = [
      'closeText' => $this->t('Close'),
      'dialogClass' => 'bankid-qr-dialog',
    ];
    $ajax_response->addCommand(
      new OpenModalDialogCommand($title, $build, $options)
    );

    return $ajax_response;
  }

  /**
   * Ajax callback to open BankID authentication on same device dialog.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   The ajax response.
   */
  public function auth(array &$form, FormStateInterface $form_state): AjaxResponse {
    $ajax_response = new AjaxResponse();
    $title = $this->t('Mobile BankID');
    $build = [
      '#theme' => 'authenticate_dialog',
    ];
    $options = [
      'closeText' => $this->t('Close'),
      'dialogClass' => 'bankid-dialog',
    ];
    $ajax_response->addCommand(
      new OpenModalDialogCommand($title, $build, $options)
    );

    return $ajax_response;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $plugin = $this->integrationManager->createInstance($this->config->get('integration'));
    $response = $form_state->getValue('response');
    /** @var \Drupal\user\UserInterface $account  */
    $account = $plugin->getUser($response);
    $redirect_path = $this->config->get('redirect_path');

    // If no user account was found and the setting to create users is enabled,
    // provision a new user.
    if (is_null($account) && $this->config->get('create_user')) {
      $account = $plugin->createUser($response);
    }

    if ($account) {
      // Redirect to the destination path if it is set.  Otherwise, redirect to
      // the configured redirect path if valid or else to the user profile.
      if ($this->getRequest()->request->has('destination')) {
        $form_state->setRedirectUrl(Url::fromUserInput($this->getRequest()->request->get('destination')));
      }
      else if (!empty($redirect_path) && strpos($redirect_path, '/') === 0) {
        $url = Url::fromUserInput($redirect_path);

        if ($url->isRouted()) {
          $form_state->setRedirectUrl($url);
        }
      }
      else {
        $form_state->setRedirect(
          'entity.user.canonical',
          ['user' => $account->id()]
        );
      }

      $authname = $this->authmap->get($account->id(), $plugin::PROVIDER_NAME);
      $this->externalAuth->userLoginFinalize($account, $authname, $plugin::PROVIDER_NAME);
    }
    else {
      $this->messenger()->addError($this->t('Unrecognized username or password.'));
    }
  }

}
