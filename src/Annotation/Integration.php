<?php

namespace Drupal\bankid\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a Integration annotation object.
 *
 * @see \Drupal\bankid\IntegrationManager
 * @see plugin_api
 *
 * @Annotation
 */
class Integration extends Plugin {
  /**
   * The human-readable name of the BankID Integration.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

}
