<?php

declare(strict_types=1);

namespace Drupal\bankid;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Site\Settings;
use Drupal\externalauth\ExternalAuthInterface;
use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\user\UserInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A base class to help developers implement their own sandwich plugins.
 *
 * @see \Drupal\bankid\Annotation\Integration
 * @see \Drupal\bankid\IntegrationInterface
 */
abstract class IntegrationBase extends PluginBase implements IntegrationInterface, ContainerFactoryPluginInterface {

  const PROVIDER_NAME = 'bankid';

  /**
   * The externalauth service.
   *
   * @var \Drupal\externalauth\ExternalAuthInterface
   */
  protected ExternalAuthInterface $externalAuth;

  /**
   * Constructs a new \Drupal\bankid\IntegrationBase object.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ExternalAuthInterface $externalAuth) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->externalAuth = $externalAuth;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('externalauth.externalauth')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getLabel(): string {
    return (string) $this->pluginDefinition['label'];
  }

  /**
   * Get user from a BankID JSON response.
   */
  abstract public function getUser(string $response): EntityInterface|NULL;

  /**
   * Create a user from a BankID JSON response.
   */
  abstract public function createUser(string $response): UserInterface;

  /**
   * Return hash of a string.
   */
  public function hash(string $str): string {
    return hash('sha256', $str . Settings::get('hash_salt', ''));
  }

}
