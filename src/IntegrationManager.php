<?php

namespace Drupal\bankid;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\bankid\Annotation\Integration;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * A plugin manager for bankid plugins.
 *
 * The IntegrationManager class extends the DefaultPluginManager to
 * provide a way to manage bankid plugins. A plugin manager defines a new
 * plugin type  and how instances of any plugin of that type will be discovered,
 * instantiated and more.
 *
 * Using the DefaultPluginManager as a starting point sets up our bankid
 * plugin type to use annotated discovery.
 *
 * The plugin manager is also declared as a service in
 * bankid.services.yml so that it can be easily accessed and used
 * anytime we need to work with bankid plugins.
 */
class IntegrationManager extends DefaultPluginManager {

  /**
   * Constructs a new \Drupal\bankid\IntegrationManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    $subdir = 'Plugin/BankID';
    $plugin_interface = IntegrationInterface::class;

    // The name of the annotation class that contains the plugin definition.
    $plugin_definition_annotation_name = Integration::class;

    parent::__construct($subdir, $namespaces, $module_handler, $plugin_interface, $plugin_definition_annotation_name);
    $this->alterInfo('bankid_integration_info');
    $this->setCacheBackend($cache_backend, 'bankid_integration_info', ['bankid_integration_info']);
  }

}
