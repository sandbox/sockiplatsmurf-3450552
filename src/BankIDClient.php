<?php

namespace Drupal\bankid;

use GuzzleHttp\Client;
use Drupal\key\KeyRepository;
use GuzzleHttp\RequestOptions;
use Drupal\Core\Config\ConfigFactory;
use GuzzleHttp\Exception\RequestException;

/**
 * Class handling BankID integration.
 *
 * @package Drupal\bankid
 */
class BankIDClient extends Client {

  /**
   * Constructs a new \Drupal\bankid\BankIDClient object.
   *
   * @param \Drupal\Core\Config\ConfigFactory $config_factory
   *   The config factory.
   * @param \Drupal\key\KeyRepository $key_repository
   *   The key repository.
   */
  public function __construct(ConfigFactory $config_factory, KeyRepository $key_repository) {
    $config = $config_factory->get('bankid.settings')->get($config_factory->get('bankid.settings')->get('environment'));
    $httpOptions = [
      'base_uri' => $config['api_base_url'] . '/',
      'cert' => [
        $key_repository->getKey($config['rp_certificate'])->getKeyProvider()->getConfiguration()['file_location'],
        $key_repository->getKey($config['rp_passphrase'])->getKeyValue(),
      ],
      'verify' => $key_repository->getKey($config['issuer_of_server_certificate'])->getKeyProvider()->getConfiguration()['file_location'],
      'headers' => [
        'Content-Type' => 'application/json',
        'Accept' => 'application/json',
      ],
    ];

    parent::__construct($httpOptions);
  }

  /**
   * Authenticate a user using animated QR code.
   *
   * @param string $endUserIp
   *   The end user IP.
   * @param string $requirement
   *   The requirement.
   * @param string $userVisibleData
   *   The user visible data.
   * @param string $userNonVisibleData
   *   The user non visible data.
   * @param string $userVisibleDataFormat
   *   The user visible data format.
   *
   * @return BankIDResponse
   *   The BankID response.
   *
   * @link https://www.bankid.com/utvecklare/guider/teknisk-integrationsguide/graenssnittsbeskrivning/auth
   */
  public function authenticate($endUserIp = '127.0.0.1', $requirement = NULL, $userVisibleData = NULL, $userNonVisibleData = NULL, $userVisibleDataFormat = NULL) {
    $payload['endUserIp'] = $endUserIp;

    if (!empty($requirement)) {
      $payload['requirement'] = $requirement;
    }

    if (!empty($userVisibleData)) {
      $payload['userVisibleData'] = base64_encode($userVisibleData);
    }

    if (!empty($userNonVisibleData)) {
      $payload['userNonVisibleData'] = base64_encode($userNonVisibleData);
    }

    if (!empty($userVisibleDataFormat)) {
      $payload['userVisibleDataFormat'] = $userVisibleDataFormat;
    }

    try {
      $httpResponse = $this->post('auth', [
        RequestOptions::JSON => $payload,
      ]);
    }
    catch (RequestException $e) {
      return $this->requestExceptionToBankIdResponse($e);
    }

    $httpResponseBody = json_decode($httpResponse->getBody(), TRUE);

    return new BankIDResponse(BankIDResponse::STATUS_PENDING, $httpResponseBody);
  }

  /**
   * Request a signing order for a user.
   *
   * @param string $endUserIp
   *   The end user IP.
   * @param string $userVisibleData
   *   The user visible data.
   * @param string $requirement
   *   The user non visible data.
   * @param string $userNonVisibleData
   *   The requirement.
   * @param string $userVisibleDataFormat
   *   The user visible data format.
   *
   * @return BankIDResponse
   *   The BankID response.
   *
   * @link https://www.bankid.com/utvecklare/guider/teknisk-integrationsguide/graenssnittsbeskrivning/sign
   */
  public function sign($endUserIp, $userVisibleData, $requirement = NULL, $userNonVisibleData = NULL, $userVisibleDataFormat = NULL) {
    try {
      $payload = [
        'endUserIp' => $endUserIp,
        'userVisibleData' => base64_encode($userVisibleData),
      ];

      if (!empty($requirement)) {
        $payload['requirement'] = $requirement;
      }

      if (!empty($userNonVisibleData)) {
        $payload['userNonVisibleData'] = base64_encode($userNonVisibleData);
      }

      if (!empty($userVisibleDataFormat)) {
        $payload['userVisibleDataFormat'] = $userVisibleDataFormat;
      }

      $httpResponse = $this->post('sign', [
        RequestOptions::JSON => $payload,
      ]);
    }
    catch (RequestException $e) {
      return $this->requestExceptionToBankIdResponse($e);
    }

    $httpResponseBody = json_decode($httpResponse->getBody(), TRUE);

    return new BankIDResponse(BankIDResponse::STATUS_PENDING, $httpResponseBody);
  }

  /**
   * Authenticate a user over phone.
   *
   * @param string $personalNumber
   *   The personal number.
   * @param string $callInitiator
   *   The call initiator.
   * @param string $requirement
   *   The requirement.
   * @param string $userVisibleData
   *   The user visible data.
   * @param string $userNonVisibleData
   *   The user non visible data.
   * @param string $userVisibleDataFormat
   *   The user visible data format.
   *
   * @return BankIDResponse
   *   The BankID response.
   *
   * @link https://www.bankid.com/utvecklare/guider/teknisk-integrationsguide/graenssnittsbeskrivning/phone-auth
   */
  public function phoneAuth($personalNumber, $callInitiator, $requirement = NULL, $userVisibleData = NULL, $userNonVisibleData = NULL, $userVisibleDataFormat = NULL) {
    try {
      $payload = [
        'personalNumber' => $personalNumber,
        'callInitiator' => $callInitiator,
      ];

      if (!empty($requirement)) {
        $payload['requirement'] = $requirement;
      }

      if (!empty($userVisibleData)) {
        $payload['userVisibleData'] = base64_encode($userVisibleData);
      }

      if (!empty($userNonVisibleData)) {
        $payload['userNonVisibleData'] = base64_encode($userNonVisibleData);
      }

      if (!empty($userVisibleDataFormat)) {
        $payload['userVisibleDataFormat'] = $userVisibleDataFormat;
      }

      $httpResponse = $this->post('phone/auth', [
        RequestOptions::JSON => $payload,
      ]);
    }
    catch (RequestException $e) {
      return $this->requestExceptionToBankIdResponse($e);
    }

    $httpResponseBody = json_decode($httpResponse->getBody(), TRUE);

    return new BankIDResponse(BankIDResponse::STATUS_PENDING, $httpResponseBody);
  }

  /**
   * Request a signing order for a user over the phone.
   *
   * @param string $personalNumber
   *   The personal number.
   * @param string $callInitiator
   *   The call initiator.
   * @param string $requirement
   *   The requirement.
   * @param string $userVisibleData
   *   The user visible data.
   * @param string $userNonVisibleData
   *   The user non visible data.
   * @param string $userVisibleDataFormat
   *   The user visible data format.
   *
   * @return BankIDResponse
   *   The BankID response.
   *
   * @link https://www.bankid.com/utvecklare/guider/teknisk-integrationsguide/graenssnittsbeskrivning/phone-sign
   */
  public function phoneSign($personalNumber, $callInitiator, $requirement = NULL, $userVisibleData = NULL, $userNonVisibleData = NULL, $userVisibleDataFormat = NULL) {
    try {
      $payload = [
        'personalNumber' => $personalNumber,
        'callInitiator' => $callInitiator,
      ];

      if (!empty($requirement)) {
        $payload['requirement'] = $requirement;
      }

      if (!empty($userVisibleData)) {
        $payload['userVisibleData'] = base64_encode($userVisibleData);
      }

      if (!empty($userNonVisibleData)) {
        $payload['userNonVisibleData'] = base64_encode($userNonVisibleData);
      }

      if (!empty($userVisibleDataFormat)) {
        $payload['userVisibleDataFormat'] = $userVisibleDataFormat;
      }

      $httpResponse = $this->post('phone/sign', [
        RequestOptions::JSON => $payload,
      ]);
    }
    catch (RequestException $e) {
      return $this->requestExceptionToBankIdResponse($e);
    }

    $httpResponseBody = json_decode($httpResponse->getBody(), TRUE);

    return new BankIDResponse(BankIDResponse::STATUS_PENDING, $httpResponseBody);
  }

  /**
   * Collect an ongoing user request.
   *
   * @param string $orderReference
   *   The order reference.
   *
   * @return BankIDResponse
   *   The BankID response.
   *
   * @link https://www.bankid.com/utvecklare/guider/teknisk-integrationsguide/graenssnittsbeskrivning/collect
   */
  public function collect($orderReference) {

    try {
      $httpResponse = $this->post('collect', [
        RequestOptions::JSON => [
          'orderRef' => $orderReference,
        ],
      ]);
    }
    catch (RequestException $e) {
      return $this->requestExceptionToBankIdResponse($e);
    }

    $httpResponseBody = json_decode($httpResponse->getBody(), TRUE);
    return new BankIDResponse($httpResponseBody['status'], $httpResponseBody);
  }

  /**
   * Cancel an ongoing order per the users request.
   *
   * @param string $orderReference
   *   The order reference.
   *
   * @return BankIDResponse
   *   The BankID response.
   *
   * @link https://www.bankid.com/utvecklare/guider/teknisk-integrationsguide/graenssnittsbeskrivning/cancel
   */
  public function cancel($orderReference) {
    try {
      $httpResponse = $this->post('cancel', [
        RequestOptions::JSON => [
          'orderRef' => $orderReference,
        ],
      ]);
    }
    catch (RequestException $e) {
      return $this->requestExceptionToBankIdResponse($e);
    }

    $httpResponseBody = json_decode($httpResponse->getBody(), TRUE);

    return new BankIDResponse(BankIDResponse::STATUS_OK, $httpResponseBody);
  }

  /**
   * Transform GuzzleHttp request exception into a BankIDResponse.
   *
   * @param GuzzleHttp\Exception\RequestException $e
   *   The request exception.
   *
   * @return BankIDResponse
   *   The BankID response.
   */
  private function requestExceptionToBankIdResponse(RequestException $e) {
    $body = $e->hasResponse() ? $e->getResponse()->getBody() : NULL;

    if ($body) {
      return new BankIDResponse(BankIDResponse::STATUS_FAILED, json_decode($body, TRUE));
    }

    return new BankIDResponse(BankIDResponse::STATUS_FAILED, ['errorMessage' => $e->getMessage()]);
  }

}
