<?php

namespace Drupal\bankid;

/**
 * BankIDResponse constructor, handling BankID response.
 *
 * @param string $status
 *   The status of the response.
 * @param null|array $body
 *   The body of the response.
 */
class BankIDResponse {

  const STATUS_OK = 'OK';
  const STATUS_COMPLETE = 'complete';
  const STATUS_PENDING = 'pending';
  const STATUS_FAILED = 'failed';

  const HINT_CODE_NO_CLIENT = 'noClient';
  const HINT_CODE_USER_CANCEL = 'userCancel';
  const HINT_CODE_EXPIRED_TRANSACTION = 'expiredTransaction';
  const HINT_CODE_USER_SIGN = 'userSign';
  const HINT_CODE_OUTSTANDING_TRANSACTION = 'outstandingTransaction';
  const HINT_CODE_STARTED = 'started';
  const HINT_CODE_CERTIFICATE_ERROR = 'certificateErr';
  const HINT_CODE_START_FAILED = 'startFailed';
  const HINT_CODE_USER_MRTD = 'userMrtd';
  const HINT_CODE_USER_CALL_CONFIRM = 'userCallConfirm';

  const ERROR_CODE_CANCELED = 'canceled';
  const ERROR_CODE_ALREADY_IN_PROGRESS = 'alreadyInProgress';
  const ERROR_CODE_REQUEST_TIMEOUT = 'requestTimeout';
  const ERROR_CODE_MAINTENANCE = 'maintenance';
  const ERROR_CODE_INTERNAL_ERROR = 'internalError';
  const ERROR_CODE_INVALID_PARAMETERS = 'invalidParameters';
  const ERROR_CODE_UNAUTHORIZED = 'unauthorized';
  const ERROR_CODE_NOT_FOUND = 'notFound';
  const ERROR_CODE_METHOD_NOT_ALLOWED = 'methodNotAllowed';
  const ERROR_CODE_UNSUPPORTED_MEDIA_TYPE = 'unsupportedMediaType';

  const RFA1 = 'RFA1';
  const RFA2 = 'RFA2';
  const RFA3 = 'RFA3';
  const RFA4 = 'RFA4';
  const RFA5 = 'RFA5';
  const RFA6 = 'RFA6';
  const RFA8 = 'RFA8';
  const RFA9 = 'RFA9';
  const RFA13 = 'RFA13';
  const RFA14A = 'RFA14-A';
  const RFA14B = 'RFA14-B';
  const RFA15A = 'RFA15-A';
  const RFA15B = 'RFA15-B';
  const RFA16 = 'RFA16';
  const RFA17A = 'RFA17-A';
  const RFA17B = 'RFA17-B';
  const RFA18 = 'RFA18';
  const RFA19 = 'RFA19';
  const RFA20 = 'RFA20';
  const RFA21 = 'RFA21';
  const RFA22 = 'RFA22';
  const RFA23 = 'RFA23';

  /**
   * BankIDResponse status.
   *
   * @var string
   */
  private string $status;

  /**
   * BankIDResponse short name.
   *
   * @var string
   */
  private string $shortName = '';

  /**
   * BankIDResponse message.
   *
   * @var string
   */
  private string $message;

  /**
   * BankIDResponse body.
   *
   * @var array|null
   */
  private ?array $body;

  /**
   * Constructs a new \Drupal\bankid\BankIDResponse object.
   *
   * @param string $status
   *   The status of the response.
   * @param null|array $body
   *   The body of the response.
   */
  public function __construct(string $status, ?array $body = NULL) {
    $this->status = $status;
    $this->body = $body;
    $this->body['status'] = $status;

    switch ($this->status) {
      case self::STATUS_PENDING:
        if (isset($body['hintCode'])) {
          switch ($body['hintCode']) {
            case self::HINT_CODE_NO_CLIENT:
              $this->shortName = self::RFA1;
              break;

            case self::HINT_CODE_USER_SIGN:
              $this->shortName = self::RFA9;
              break;

            case self::HINT_CODE_OUTSTANDING_TRANSACTION:
              $this->shortName = self::RFA1;
              break;

            case self::HINT_CODE_STARTED:
              $this->shortName = self::RFA14B;
              break;

            case self::HINT_CODE_USER_MRTD:
              $this->shortName = self::RFA23;
              break;

            default:
              $this->shortName = self::RFA21;
              break;
          }
        }
        break;

      case self::STATUS_FAILED:
        if (isset($body['hintCode'])) {
          switch ($body['hintCode']) {
            case self::HINT_CODE_USER_CANCEL:
              $this->shortName = self::RFA6;
              break;

            case self::HINT_CODE_EXPIRED_TRANSACTION:
              $this->shortName = self::RFA8;
              break;

            case self::HINT_CODE_CERTIFICATE_ERROR:
              $this->shortName = self::RFA16;
              break;

            case self::HINT_CODE_START_FAILED:
              $this->shortName = self::RFA17B;
              break;

            default:
              $this->shortName = self::RFA22;
              break;
          }
        }
        elseif (isset($body['errorCode'])) {
          switch ($body['errorCode']) {
            case self::ERROR_CODE_CANCELED:
              $this->shortName = self::RFA3;
              break;

            case self::ERROR_CODE_ALREADY_IN_PROGRESS:
              $this->shortName = self::RFA4;
              break;

            case self::ERROR_CODE_REQUEST_TIMEOUT:
            case self::ERROR_CODE_MAINTENANCE:
            case self::ERROR_CODE_INTERNAL_ERROR:
              $this->shortName = self::RFA5;
              break;

            default:
              $this->shortName = self::RFA22;
              break;
          }
        }
        break;
    }
    $this->message = $this->shortName;
    $this->body['shortName'] = $this->shortName;
    $this->body['message'] = BankIDUserMessages::getMessage($this->shortName);
  }

  /**
   * Get the short name.
   */
  public function getShortName(): string {
    return $this->shortName;
  }

  /**
   * Get the message.
   */
  public function getMessage(): string {
    return $this->message;
  }

  /**
   * Get the body.
   */
  public function getBody(): ?array {
    return $this->body;
  }

  /**
   * Get the status.
   */
  public function getStatus(): string {
    return $this->status;
  }

  /**
   * Get the order reference.
   */
  public function getOrderRef(): ?string {
    return $this->body['orderRef'] ?? NULL;
  }

  /**
   * Get the personalNumber.
   */
  public function getPersonalNumber(): ?string {
    return $this->body['completionData']['user']['personalNumber'] ?? NULL;
  }

  /**
   * Get the error code.
   */
  public function getErrorCode(): ?string {
    return $this->body['errorCode'] ?? NULL;
  }

  /**
   * Get the details.
   */
  public function getErrorDetails(): ?string {
    return $this->body['details'] ?? NULL;
  }

  /**
   * Get the hint code.
   */
  public function getHintCode(): ?string {
    return $this->body['hintCode'] ?? NULL;
  }

  /**
   * Get the auto start token.
   */
  public function getAutoStartToken(): ?string {
    return $this->body['autoStartToken'] ?? NULL;
  }

}
