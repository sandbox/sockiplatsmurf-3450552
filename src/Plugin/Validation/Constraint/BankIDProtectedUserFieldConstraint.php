<?php

namespace Drupal\bankid\Plugin\Validation\Constraint;

use Drupal\user\Plugin\Validation\Constraint\ProtectedUserFieldConstraint;

/**
 * Checks if the plain text password is provided for editing a protected field.
 *
 * @Constraint(
 *   id = "BankIDProtectedUserField",
 *   label = @Translation("Password required for protected field change unless user provisioned by BankID", context = "Validation")
 * )
 */
class BankIDProtectedUserFieldConstraint extends ProtectedUserFieldConstraint {

}
