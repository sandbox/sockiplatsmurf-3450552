<?php

namespace Drupal\bankid\Plugin\Validation\Constraint;

use Drupal\bankid\Plugin\BankID\DefaultIntegration;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\externalauth\AuthmapInterface;
use Drupal\user\Plugin\Validation\Constraint\ProtectedUserFieldConstraintValidator;
use Drupal\user\UserStorageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Validator\Constraint;

/**
 * Decorates the ProtectedUserFieldConstraint constraint.
 */
class BankIDProtectedUserFieldConstraintValidator extends ProtectedUserFieldConstraintValidator {

  /**
   * The authmap service.
   *
   * @var \Drupal\externalauth\AuthmapInterface
   */
  protected AuthmapInterface $authmap;

  /**
   * BankIDProtectedUserFieldConstraintValidator constructor.
   */
  public function __construct(UserStorageInterface $user_storage, AccountProxyInterface $current_user, AuthmapInterface $authmap) {
    parent::__construct($user_storage, $current_user);
    $this->authmap = $authmap;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')->getStorage('user'),
      $container->get('current_user'),
      $container->get('externalauth.authmap')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function validate($items, Constraint $constraint) {
    if (!empty($items)) {
      $account = $items->getEntity();
      if ($account->isNew()) return;
      $provisioned = $this->authmap->get($account->id(), DefaultIntegration::PROVIDER_NAME);
      if ($provisioned) {
        return;
      }
    }

    parent::validate($items, $constraint);
  }

}
