<?php

declare(strict_types=1);

namespace Drupal\bankid\Plugin\BankID;

use Drupal\bankid\IntegrationBase;
use Drupal\Core\Entity\EntityInterface;
use Drupal\user\UserInterface;

/**
 * Provides default integration.
 *
 * @Integration(
 *   id = "default",
 *   label = @Translation("Default integration.")
 * )
 */
class DefaultIntegration extends IntegrationBase {

  /**
   * {@inheritdoc}
   */
  public function getUser(string $response): EntityInterface|NULL {
    $r_array = json_decode($response, TRUE);
    $authname = $this->hash($r_array['completionData']['user']['personalNumber']);

    return $this->externalAuth->load($authname, self::PROVIDER_NAME) ?: NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function createUser(string $response): UserInterface {
    $r_array = json_decode($response, TRUE);
    $ssn = $r_array['completionData']['user']['personalNumber'];
    $authname = $this->hash($ssn);

    // Username has a character limit of 60. So we can't use the hashed SSN as
    // the username.
    $name = hash('md5', $authname);

    return $this->externalAuth->register($authname, self::PROVIDER_NAME, ['name' => $name]);
  }

}
