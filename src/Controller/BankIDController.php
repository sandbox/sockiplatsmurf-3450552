<?php

namespace Drupal\bankid\Controller;

use Drupal\bankid\BankIDClient;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * BankID controller.
 */
class BankIDController extends ControllerBase {

  /**
   * The BankID service.
   *
   * @var \Drupal\bankid\BankIDClient
   */
  protected $bankID;

  /**
   * Constructs a new \Drupal\bankid\Controller\BankIDController object.
   *
   * @param \Drupal\bankid\BankIDClient $bankid
   *   The BankID service.
   */
  public function __construct(BankIDClient $bankid) {
    $this->bankID = $bankid;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('bankid')
    );
  }

  /**
   * Initiates an authentication order.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The JSON response.
   */
  public function authenticate() {
    $result = $this->bankID->authenticate();
    return new JsonResponse($result->getBody());
  }

  /**
   * Collects the result of a sign or auth order using orderRef as reference.
   *
   * @param string $orderRef
   *   The order reference.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The JSON response.
   */
  public function collect($orderRef) {
    $result = $this->bankID->collect($orderRef);
    return new JsonResponse($result->getBody());
  }

  /**
   * Cancels an ongoing order per the users request.
   *
   * @param string $orderRef
   *   The order reference.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The JSON response.
   */
  public function cancel($orderRef) {
    $result = $this->bankID->cancel($orderRef);
    return new JsonResponse($result->getBody());
  }

}
